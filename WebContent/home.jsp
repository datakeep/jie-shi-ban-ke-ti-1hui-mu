<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	 <link href="./css/all.css" rel="stylesheet" type="text/css">
    	 <title>ホーム画面</title>
		 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
    	<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
					<c:remove var="errorMessages" scope="session" />
				</ul>
			</div>
		</c:if>
        <div class="main-contents">
            <div class="header">
            	<a href="message">新規投稿</a>
	            	<c:if test = "${loginUser.branchId == 1 }">
	                	<a href="management">ユーザー管理</a>
	                </c:if>
                <a href="logout">ログアウト</a>
                <div class="profile">
					<div class="account">アカウント名:<c:out value="${loginUser.account}" /></div>
					<div class="name">ログイン名:<c:out value="${loginUser.name}" /></div><br />
				</div>
            </div>
        </div>
       	<form action="index.jsp" method="get">
       		<label for="date">日付指定</label>
       		<input type="date" name="start" id="start" value="${start}">
       		<input type="date" name="end" id="end" name="end" value="${end}"><br/><br/>
       		<label for="category">カテゴリ検索</label>
       		<input name="category" id="category" value="${category}"><br/><br/>
       		<input type="submit" value="絞り込み"><br/><br/>
       	</form>
        <div class="messages">
    		<c:forEach items="${messages}" var="message">
       			 <div class="message">
		            <div class="account-name">
		                ユーザー名:<div class="name"><c:out value="${message.name}" /></div><br/>
		                件名:<div class="title"><c:out value="${message.title}" /></div><br/>
		                カテゴリ:<div class="category"><c:out value="${message.category}" /></div><br/>
		                投稿内容:<div class="text"><pre><c:out value="${message.text}" /></pre></div><br/>
		            </div>
            		<div class="date">
            			投稿日時:<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /><br/>
            		</div>
            		<c:if test="${ loginUser.id == message.userId }">
            			<form action="deleteMessage" method="post">
							<input type="hidden" name="messageId" value="${message.id}">
            				<input type="submit" value="消去" onClick="confirm2(); return false"/>
            			</form>
            		</c:if>
            		<c:forEach items="${comments}" var="comment">
            			<c:if test="${comment.messageId == message.id}">
				                ユーザー名:<div class="name"><c:out value="${comment.name}" /></div><br/>
				                コメント内容:<div class="text"><pre><c:out value="${comment.text}" /></pre></div><br/>
				                コメント日時:<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /><br/>
				            <c:if test="${ loginUser.id == comment.userId }">
				            	<form action="deleteComment" method="post">
				            		<input type="hidden" name="commentId" value="${comment.id}" id="id">
	            					<input type="submit" value="消去" onClick="confirm2(); return false"/>
	            				</form>
	            			</c:if>
			            </c:if>
		            </c:forEach>
            		<form action="comment" method="post">
           				<label for = "comment">コメント投稿</label>
						<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
						<input type="hidden" name="messageId" value="${message.id}"><br/>
            			<input type="submit" value="コメント投稿" >
            		</form>
            	</div>
			</c:forEach>
		</div>
		<div class="copyright"> Copyright(c)SaitoKana</div>
		<script>
			<!-- 投稿消去 -->
			function confirm2(){
				if(window.confirm('本当によろしいですか？')){
					message.check().submit();
					return true;
				}
			}
		</script>
    </body>
</html>