<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	 <link href="./css/all.css" rel="stylesheet" type="text/css">
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <title>ユーザー管理</title>
    </head>
    <body>
	    <div class="header">
	    	<a href="./">ホーム</a>
			<a href="signup">ユーザー新規登録</a><br />
		</div>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                        <c:remove var="errorMessages" scope="session" />
                    </ul>
                </div>
            </c:if>
			<div class="users">
				<c:forEach items="${users}" var="user">
		           	 アカウント<span class="account"><c:out value="${user.account}" /></span><br/>
		             名前<span class="name"><c:out value="${user.name}" /></span><br/>
		             支社<span class="branchName"><c:out value="${user.branchName}" /></span><br/>
		             部署<span class="departmentName"><c:out value="${user.departmentName}" /></span><br/>
		             アカウント復活停止状態<span class="isStopped"><c:out value="${user.isStopped}" /></span><br/>

				<form action="stop" method="post">
					<c:if test="${loginUser != user.id}">
	            		<input type="hidden" name="userId" value="${user.id}" id="id">
		            		<c:if test="${user.isStopped == 1}">
		            			<input type="hidden" name="isStopped" value="0" id="id">
	          					<input type="submit" value="復活" onClick="confirm1(); return false"/>
	          				</c:if>
            			<input type="hidden" name="userId" value="${user.id}">
            			<c:if test="${user.isStopped == 0}">
	           				<input type="hidden" name="isStopped" value="1" id="id">
	        				<input type="submit" value="停止" onClick="confirm1(); return false"/>
         				</c:if>
         			</c:if>
         		</form>
            	<form action="setting" method="get"><br />
	           		<input type="hidden" name="userId" value="${user.id}">
            		<br><input type="submit" value="編集" /> <br />
            	</form>
               	</c:forEach>
            </div>
            <div class="copyright">Copyright(c)SaitoKana</div>
        </div>
        <script>
        	<!-- 停止復活確認 -->
	        function confirm1(){
				if(window.confirm('本当によろしいですか？')){
					re.check().submit();
					return true;
				}
			}
		</script>
    </body>
</html>