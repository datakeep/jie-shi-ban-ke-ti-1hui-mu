<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板課題</title>
		<link href="./css/all.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="form-area">
				<form action="message" method="post">
					<a href="./">ホーム</a><br />
					<label for = "title">件名</label>
					<input name = "title" value = "${title}" id = "title" /><br />
					<label for = "category">カテゴリ</label>
					<input name = "category" value = "${category}" id = "category" /><br />
					<label for = "text">投稿内容</label>
					<textarea name="text" cols="100" rows="5" class="tweet-box">${text}</textarea><br />
					<input type="submit" value="投稿"><br />
				</form>
			</div>

			<div class="copylight"> Copyright(c)SaitoKana</div>
		</div>
	</body>
</html>