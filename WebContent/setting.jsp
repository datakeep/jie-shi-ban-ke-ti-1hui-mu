<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
     	<link href="./css/all.css" rel="stylesheet" type="text/css">
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <title>ユーザー編集</title>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

			<form action="setting" method="post"><br />
            	<label for="account">アカウント名</label>
                <input name="account" value="${user.account}" id="account" /> <br />

               	<label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

                <label for="confirmedpassword">確認用パスワード</label>
                <input name="confirmedPassword" type="password" id="confirmedPassword" /> <br />

                <label for="name">名前</label>
                <input name="name" value="${user.name}" id="name" /><br />


				<c:if test="${loginUser.id != user.id}">
	               <label for = "branches">支社</label>
	               	<select name="branchId" size="1" id="branchId">
	               		<c:forEach items="${branches}" var="branch">

	               			<%--支社IDとユーザーの支社IDが一致しなかったら選択式--%>
	               			<c:if test="${branch.id != user.branchId}">
	               				<option value="${branch.id}"> ${branch.name}</option>
	               			</c:if>
	               			<%--支社IDとユーザーの支社IDが一致したら選択肢の中に支社IDを表示しておく--%>
	               			<c:if test="${branch.id == user.branchId}">
	               				<option value="${branch.id}" selected> ${branch.name}</option>
	               			</c:if>
	               		</c:forEach>
	               	</select>
                </c:if>

                <%--ログインしている人と変更しようとしているユーザーが一緒の場合支社が変更できない--%>
                <c:if test="${loginUser.id == user.id}">
                	<label for = "branches">支社</label>
	               	<select name="branchId">
	               		<c:forEach items="${branches}" var="branch">
	               			<c:if test="${branch.id == user.branchId}">
	               				<option selected value="${branch.id}"> ${branch.name}</option>
	               			</c:if>
	               		</c:forEach>
	               	</select>
                </c:if>

				<c:if test="${ loginUser.id != user.id }">
				<label for ="departments">部署</label>
                	<select name="departmentId" size="1" id="departmentId">
                		<c:forEach items="${departments}" var="department">

                			<c:if test="${department.id == user.departmentId}">
                				<option value = "${department.id}" selected>${department.name}</option>
                			</c:if>
                			<c:if test="${department.id != user.departmentId}">
                				<option value = "${department.id}">${department.name}</option>
                			</c:if>
                		</c:forEach>
                	</select>
                </c:if>

                <%--ログインしている人と変更しようとしているユーザーが一緒の場合部署が変更できない--%>
                <c:if test="${loginUser.id == user.id}">
                	<label for ="departments">部署</label>
                	<select name="departmentId">
                		<c:forEach items="${departments}" var="department">
                			<c:if test="${department.id == user.departmentId}">
                				<option selected value = "${department.id}">${department.name}</option>
                			</c:if>
                		</c:forEach>
                	</select>
                </c:if>
                <input type="hidden" name="userId" value="${user.id}">
                <input type="hidden" name="confirmAccount" value="${user.account}">
                <input type="submit" value="更新" /> <br />
       		</form>

            <div class="copyright">Copyright(c)SaitoKana</div>
        </div>
    </body>
</html>