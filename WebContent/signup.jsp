<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	 <link href="./css/all.css" rel="stylesheet" type="text/css">
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <title>ユーザー登録</title>
    </head>
    <body>
		<c:if test="${ not empty errorMessages }">
		     <div class="errorMessages">
		         <ul>
		             <c:forEach items="${errorMessages}" var="errorMessage">
		                 <li><c:out value="${errorMessage}" />
		             </c:forEach>
		         </ul>
		     </div>
		</c:if>

		<div class="main-contents">
            <form action="signup" method="post"><br />

            	<a href="./">ユーザー管理</a><br />
            	<label for="account">アカウント名</label>
                <input name="account" id="account" value="${account}"/> <br />

               	<label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

                <label for="confirmedpassword">確認用パスワード</label>
                <input name="confirmedPassword" type="password" id="confirmedPassword" /> <br />

                <label for="name">ユーザ名</label>
                <input name="name" value="${name}" id="name" /><br />

                <p>支社<br>
                	<select size ="1" name="branchId" id="branchId">
                		<c:forEach items="${branches}" var="branch">
                			<c:if test="${branch.id == branchId}">
                				<option value="${branch.id}" selected> ${branch.name}</option>
                			</c:if>
                			<c:if test="${branch.id != branchId}">
                				<option value="${branch.id}"> ${branch.name}</option>
                			</c:if>
                		</c:forEach>
                	</select>
                </p>

				<p>部署<br>
                	<select size="1" name="departmentId" id="departmentId">
                		<c:forEach items="${departments}" var="department">
                			<c:if test="${department.id == departmentId}">
                				<!-- selectedで保持したままにできる -->
                				<option value = "${department.id}" selected>${department.name}</option>
                			</c:if>
                			<c:if test="${department.id != departmentId}">
                				<option value = "${department.id}">${department.name}</option>
                			</c:if>
                		</c:forEach>
                	</select>
				</p>

                <input type="submit" value="登録" /> <br />
            </form>

            <div class="copyright">Copyright(c)SaitoKana</div>
        </div>
    </body>
</html>