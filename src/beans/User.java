package beans;

import java.io.Serializable;
import java.sql.Date;

public class User implements Serializable{
    private String account;
    private String password;
    private String confirmedPassword;
    private String name;
    private int id;
    private int isStopped;
    private int branchId;
    private int departmentId;
    private Date createdDate;
    private Date updatedDate;

    // getter/setterは省略されているので、自分で記述しましょう
    public void setAccount(String account) {
    	this.account = account;
    }
    public String getAccount() {
        return account;
    }

    public void setPassword(String password) {
    	this.password = password;
    }
    public String getPassword() {
        return password;
    }
    public void setConfirmedPassword(String confirmedPassword) {
    	this.confirmedPassword = confirmedPassword;
    }
    public String getConfirmedPassword() {
        return confirmedPassword;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }
    public int getBranchId() {
        return branchId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
    public int getDepartmentId() {
        return departmentId;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
    public Date getUpdatedDate() {
        return updatedDate;
	}
	public int getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}
}
