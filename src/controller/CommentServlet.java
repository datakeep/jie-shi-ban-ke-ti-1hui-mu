package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comments;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet{
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();


        String text = request.getParameter("text");

        if (!isValid(request, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            session.setAttribute("text", text);
            response.sendRedirect("./");
            return;
        }

        Comments comment = new Comments();
        comment.setText(text);

        User user = (User)session.getAttribute("loginUser");
        comment.setUserId(user.getId());

        int messageId = (Integer.parseInt(request.getParameter("messageId")));
        comment.setMessageId(messageId);

        new CommentService().insert(comment);
        response.sendRedirect("./");
	}

	 private boolean isValid(HttpServletRequest request, List<String> errorMessages) {
		 String text = request.getParameter("text");

        if (StringUtils.isBlank(text)) {
            errorMessages.add("コメントを入力してください");
        } else if (!(text.length() <= 500)){
            errorMessages.add("500字以下で入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
	}
}