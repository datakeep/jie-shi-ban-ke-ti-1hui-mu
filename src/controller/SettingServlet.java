package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branches;
import beans.Departments;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    	throws ServletException, IOException {

    	List<String> errorMessages = new ArrayList<String>();
    	String userId = request.getParameter("userId");
    	HttpSession session = ((HttpServletRequest)request).getSession();

    	if (!userId.matches("^[0-9]+$")){
            errorMessages.add("不正なパラメータが入力されました");
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("management");
            return;
        }

        User user = new UserService().select(Integer.parseInt(request.getParameter("userId")));

        if (user == null){
            errorMessages.add("不正なパラメータが入力されました");
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("management");
            return;
        }

        List<Branches> branches = new BranchService().select();
        List<Departments> departments = new DepartmentService().select();


        request.setAttribute("user", user);
    	request.setAttribute("branches", branches);
    	request.setAttribute("departments", departments);
        request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    	throws ServletException, IOException {

        List<String> errorMessages = new ArrayList<String>();
        List<Branches> branches = new BranchService().select();
        List<Departments> departments = new DepartmentService().select();

        User user = getUser(request);
        if (!isValid(request, user, errorMessages)) {
        	/*request.setAttribute("account", user.getAccount());
        	request.setAttribute("name", user.getName());
        	request.setAttribute("password", user.getPassword());*/

        	request.setAttribute("user", user);
	    	request.setAttribute("branches", branches);
	    	request.setAttribute("branchId", user.getBranchId());
	    	request.setAttribute("departments", departments);
	    	request.setAttribute("departmentId", user.getDepartmentId());
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

        new UserService().update(user);
        response.sendRedirect("management");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("userId")));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setConfirmedPassword(request.getParameter("confirmedPassword"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        return user;
    }

    private boolean isValid(HttpServletRequest request, User user, List<String> errorMessages) {

    	String account = user.getAccount();
        String password = user.getPassword();
        String confirmedPassword = user.getConfirmedPassword();
        String name = user.getName();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();
        User human = new UserService().select(user.getAccount());

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (!((account.matches("^[a-zA-Z0-9]+$")) && ((6 <= account.length()) && (account.length() <= 20)))){
            errorMessages.add("アカウント名は半角英数字で6文字以上20文字以下で入力してください");
        }
        if(human != null) {
        	errorMessages.add("アカウントが重複しています");
        }
        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        } else if (!((password.matches("^[-_@+*;:#$%&A-Za-z0-9]+$")) && ((6 <= password.length()) && (password.length() <= 20)))){
            errorMessages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
        }
        if (StringUtils.isEmpty(confirmedPassword)) {
            errorMessages.add("確認用パスワードを入力してください");
        } else if (!((confirmedPassword.matches("^[-_@+*;:#$%&A-Za-z0-9]+$")) && ((6 <= confirmedPassword.length()) && (confirmedPassword.length() <= 20)))){
            errorMessages.add("確認用パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
        }
        if(password.equals(confirmedPassword)) {
        	errorMessages.add("パスワードが一致しません");
        }
        if (StringUtils.isEmpty(name)){
            errorMessages.add("名前を入力してください");
        }else if (!( name.length() <= 10)) {
        	errorMessages.add("名前は10文字以下で入力してください");
        }
        if (!(((branchId == 1) && ((departmentId == 1) || (departmentId == 2))) || (((branchId != 1) && ((departmentId == 3) || (departmentId == 4))))))  {
			errorMessages.add("支店と部署の組み合わせが正しくありません");
		}
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
