package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comments;
import exception.SQLRuntimeException;

public class CommentDao {
	public void insert(Connection connection, Comments comment) {

	    PreparedStatement ps = null;
	    try {
	      StringBuilder sql = new StringBuilder();
	            sql.append("INSERT INTO comments ( ");
	            sql.append("    id, ");
	            sql.append("    user_id, ");
	            sql.append("    text, ");
	            sql.append("    message_id, ");
	            sql.append("    created_date, ");
	            sql.append("    updated_date ");
	            sql.append(") VALUES ( ");
	            sql.append("    ?, ");
	            sql.append("    ?, ");                                  // user_id
	            sql.append("    ?, ");
	            sql.append("    ?, ");
	            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
	            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
	            sql.append(")");

	      ps = connection.prepareStatement(sql.toString());

	      ps.setInt(1, comment.getId());
	      ps.setInt(2, comment.getUserId());
	      ps.setString(3, comment.getText());
	      ps.setInt(4, comment.getMessageId());

	      ps.executeUpdate();

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public void delete(Connection connection, int commentId) {

	    PreparedStatement ps = null;
	    try {
	      StringBuilder sql = new StringBuilder();
	            sql.append("DELETE FROM comments WHERE id = ? ");

	      ps = connection.prepareStatement(sql.toString());

	      ps.setInt(1, commentId);

	      ps.executeUpdate();

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
}
