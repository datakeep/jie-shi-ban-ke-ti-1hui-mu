package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		String requestPath = ((HttpServletRequest)request).getServletPath();
		List<String> errorMessages = new ArrayList<String>();

		if(!(requestPath.equals("/login")) && !(requestPath.contains("/css"))) {
			User loginUser = (User) session.getAttribute("loginUser");
			if(loginUser == null) {
				errorMessages.add("ログインしてください");
				session.setAttribute("errorMessages", errorMessages);
				((HttpServletResponse)response).sendRedirect("login");//ログイン画面へ遷移
				return;
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) throws ServletException{
	}

	@Override
	public void destroy() {
	}
}