package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter({"/signup", "/setting", "/management"})
public class ManagementFilter implements Filter{


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		int branchId = loginUser.getBranchId();
		int departmentId = loginUser.getDepartmentId();
		List<String> errorMessages = new ArrayList <String>();

		//ユーザー管理画面(management)、ユーザー編集画面(setting)、ユーザー新規登録画面()

		if(branchId != 1 || departmentId != 1){//ログインしてるユーザが本社の総務人事部じゃないとき
			errorMessages.add("管理者権限がありません");
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse)response).sendRedirect("./");
			return;
		}
		chain.doFilter(request, response); // サーブレットを実行
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}
